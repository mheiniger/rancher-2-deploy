# Rancher 2.x Deploy Tool

Deployment tool for updating workloads on Rancher 2.x managed cluster with v3 API. It was developed for gitlab CD pipeline, but can be used for any other workflow. Influenced by the project [rancher-gitlab-deploy](https://github.com/cdrx/rancher-gitlab-deploy).

It can be used as CLI tool, Node.js module or CI/CD pipeline tool as a docker image. It can be easily integrated into `gitlab-ci.yml` or `.rancher-pipeline.yml` using environment variables.

Right now it's only possible to redeploy a workload, so first you'll need to deploy your workload manually.

## Usage

First you need to get an API key from your Rancher Cluster. In Rancher go to **User Avatar** > **API & Keys** > **Add Key** ([more info](https://rancher.com/docs/rancher/v2.x/en/user-settings/api-keys/)).

### Gitlab CI/CD Pipeline

Save the key as an environment variable in your GitLab project. On the GitLab project page go to **Settings** > **CI / CD** > **Variables** ([more info](https://gitlab.com/help/ci/variables/README#variables)).

`RANCHER_ENDPOINT` - https://rancher.example.com/v3

`RANCHER_ACCESS_KEY` - token-12345

`RANCHER_SECRET_KEY` - 0123456789abcdefghijklmnopqrstuvwxyz

Deploy your project on Rancher GUI under Default project to default namespace, then put a deploy stage to your `gitlab-ci.yml`:
```
deploy:
  stage: deploy
  image: registry.gitlab.com/heyj0e/rancher-2-deploy
  script:
    - deploy --workload <workload-name>
```

If you want to specify the project and/or the namespace:
```
deploy:
  stage: deploy
  image: registry.gitlab.com/heyj0e/rancher-2-deploy
  script:
    - deploy --project <project-name> --namespace <namespace-name> --workload <workload-name>
```

### Node.js module

Install it using npm:
```
npm install rancher-2-deploy --save
```

Use:
```
const rancherRedeploy = require('rancher-2-deploy');

const options = {
  endpoint: 'https://myserver.com/v3',
  accessKey: 'token-12345',
  secretKey: '0123456789abcdefghijklmnopqrstuvwxyz',
  project: 'My Rancher Project', // if omitted, Default will be used
  namespace: 'My Namespace', // if omitted, default will be used
  workload: 'my-awesome-workload',
};

rancherRedeploy(options);
// returns Promise, then Rancher's answer
```

## Authors

* **Joseph Ivan** - *Initial work* - [HeyJ0e](https://gitlab.com/HeyJ0e)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
