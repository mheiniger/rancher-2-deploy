FROM node:8.11.3-alpine

ADD . /usr/rancher-2-deploy
WORKDIR /usr/rancher-2-deploy

RUN npm install -y --only=prod && \
  npm link

CMD deploy
