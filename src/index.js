#!/usr/bin/env node
/*eslint no-console: "off"*/

const argv = require('minimist')(process.argv.slice(2));
const request = require('request-promise-native');
const urlJoin = require('url-join');

const RANCHER = { };


if (require.main === module) {
  main();
}

async function main() {
  try {
    let result = await redeploy();
    console.log(`Successfully updated ${result.name}.`);
    process.exit(0);
  } catch(err) {
    console.error(err.toString());
    process.exit(1);
  }
}

/**
 * Redeploy a rancher workload changing the timestamp annotation. This will
 * force rancher update the containers.
 * @param {object} options
 * @param {string} options.project Rancher project name
 * @param {string} options.namespace Cluster namespace name
 * @param {string} options.workload Workload name
 * @param {string} options.endpoint Rancher api endpoint (e.g.: https://example.com/v3)
 * @param {string} options.accessKey Rancher api access key
 * @param {string} options.secretKey Rancher api secret key
 */
async function redeploy(options={}) {
  const project = options.project || argv.project || process.env.RANCHER_PROJECT || 'Default';
  const namespace = options.namespace || argv.namespace || process.env.RANCHER_NAMESPACE || 'default';
  const workload = options.workload || argv.workload || process.env.RANCHER_WORKLOAD;

  RANCHER.endpoint = options.endpoint || argv.endpoint || process.env.RANCHER_ENDPOINT;
  RANCHER.accessKey = options.accessKey || argv.accesskey || process.env.RANCHER_ACCESS_KEY;
  RANCHER.secretKey = options.secretKey || argv.secretkey || process.env.RANCHER_SECRET_KEY;

  if (!workload) throw new Error('Workload must be specified.');
  if (!RANCHER.endpoint) throw new Error('Rancher endpoint must be specified.');
  if (!RANCHER.accessKey) throw new Error('Rancher accessKey must be specified.');
  if (!RANCHER.secretKey) throw new Error('Rancher secretKey must be specified.');

  const projectData = await getProject(project);
  const workloadData = await getWorkload(projectData, workload, namespace);
  const uploadResult = await updateWorkload(workloadData);

  return uploadResult;
}

async function getProject(project) {
  const url = urlJoin(RANCHER.endpoint, 'project', `?name=${project}`);
  const requestOptions = {
    auth: {
      user: RANCHER.accessKey,
      pass: RANCHER.secretKey,
    },
    json: true,
  };

  const response = await request.get(url, requestOptions);

  if (!response.data || response.data.length === 0) {
    throw new Error(`Project ${project} not found.`);
  }

  return response.data[0];
}

async function getWorkload(projectData, workload, namespace) {
  const url = urlJoin(projectData.links.workloads, `?name=${workload}&namespaceId=${namespace}`);
  const requestOptions = {
    auth: {
      user: RANCHER.accessKey,
      pass: RANCHER.secretKey,
    },
    json: true,
  };

  const response = await request.get(url, requestOptions);

  if (!response.data || response.data.length === 0) {
    throw new Error(`Workload ${workload} not found in namespace ${namespace}.`);
  }

  return response.data[0];
}

async function updateWorkload(workloadData) {
  const url = workloadData.links.update;

  const workloadUpdated = Object.assign({}, workloadData);

  if (!workloadUpdated.annotations ||
    !workloadUpdated.annotations['cattle.io/timestamp'])
  {
    throw new Error('Workload data error: missing annotations.');
  }

  workloadUpdated.annotations['cattle.io/timestamp'] = new Date().toISOString();

  const requestOptions = {
    auth: {
      user: RANCHER.accessKey,
      pass: RANCHER.secretKey,
    },
    json: workloadUpdated,
  };

  return request.put(url, requestOptions);
}


module.exports = redeploy;
